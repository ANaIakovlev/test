const { src, dest, watch, parallel, series } = require("gulp");
const sass = require("gulp-sass")(require("sass"));
const concat = require("gulp-concat");
const uglify = require("gulp-uglify-es").default;
const browserSync = require("browser-sync").create();
const autoprefixer = require("gulp-autoprefixer");
const clean = require("gulp-clean");

function scripts() {
	return src([
		"node_modules/swiper/swiper-bundle.js",
		"app/js/index.js"
		// "app/js/*.js" for all js files
		// "!app/js/index.min.js" ignore for up command
	])
		.pipe(concat("index.min.js"))
		.pipe(uglify()) // less
		.pipe(dest("app/js"))
		.pipe(browserSync.stream()); // auto reload
}

function styles() {
	return src("app/sass/styles.sass")
		.pipe(autoprefixer({ overrideBrowserslist: ["last 10 version"] })) // добавляет префиксы для версий браузеров
		.pipe(concat("styles.min.css")) // rename
		.pipe(sass({ outputStyle: "compressed" })) // min 
		.pipe(dest("app/css"))
		.pipe(browserSync.stream()); // auto reload
}

function watching() {
	watch(["app/sass/styles.sass"], styles); // при изменении файла будет вызваться styles
	watch(["app/js/index.js"], scripts);
	watch(["app/*.html"]).on("change", browserSync.reload); // *.html - all html files
}

function sync() {
	browserSync.init({
		server: {
			baseDir: "app/"
		}
	});
}

function cleanDist() {
	return src("dist")
		.pipe(clean());
}

function building() {
	return src([
		"app/css/styles.min.css",
		"app/js/index.min.js",
		"app/**/*.html" // from all folders
	], { base: "app" }) // save folders structure
		.pipe(dest("dist")); // to dist folder
}

exports = {
	styles,
	scripts,
	watching,
	browserSync,
	build: series(cleanDist, building) // сначала очистка диста, затем билд
};
exports.default = parallel(styles, scripts, browserSync, sync, watching); // запускаем параллельно